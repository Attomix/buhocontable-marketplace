class PaymentSerializer < ActiveModel::Serializer
  attributes  :date_payment, :amount

  has_many :subscription_histories

end
