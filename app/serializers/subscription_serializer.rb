class SubscriptionSerializer < ActiveModel::Serializer
  attributes :id, :name, :price
  has_many :subscription_histories
  has_many :services
  has_many :products

end
