class SubscriptionHistorySerializer < ActiveModel::Serializer
  attributes  :period, :date_subscription, :status, :subscription
  belongs_to :subscription
  has_many :payments
end
