require 'core/subscriptions/client_service'
class Api::V1::ClientsController < ApplicationController
  before_action :set_client, only: [:payment_history]
  def create

  end

  def payment_history
    json_response(MarketPlace::ClientService.new.payment_histories(@client))
  end

  private
  def set_client
    @client = Client.find(params[:id])
  end

end
