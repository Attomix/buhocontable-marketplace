require 'core/subscriptions/subscription_history_service'

class Api::V1::SubscriptionHistoriesController < ApplicationController

  def create
    json_response( MarketPlace::SubscriptionHistoryService.new.subscribe(subscription_history_params), :created)
  end

  private
  def subscription_history_params
    params.require(:subscription_history).permit(:client_id, :subscriptions => [:id, :period])
  end

end
