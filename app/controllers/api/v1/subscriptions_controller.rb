class Api::V1::SubscriptionsController < ApplicationController
  def index
    json_response(Subscription.active)
  end
end
