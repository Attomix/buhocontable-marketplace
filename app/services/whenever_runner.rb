class WheneverRunner
  def self.update
    begin
      puts "after create subscription update crontab"
      cmd = "whenever --update-crontab"
      system( cmd )
    rescue Exception => e
      puts e.message
    end
  end
end