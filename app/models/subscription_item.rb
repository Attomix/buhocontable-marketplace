class SubscriptionItem < ApplicationRecord
  belongs_to :subscription
  belongs_to :itemeable, polymorphic: true
end
