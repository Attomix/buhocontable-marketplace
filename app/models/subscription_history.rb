class SubscriptionHistory < ApplicationRecord
  belongs_to :client
  belongs_to :subscription
  has_and_belongs_to_many :payments

  enum status: { active: true, inactive: false }

  enum period: { monthly: 1, annual: 2 }

  validates_presence_of :period, :date_subscription, :status
  accepts_nested_attributes_for :subscription

end
