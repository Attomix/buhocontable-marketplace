class Subscription < ApplicationRecord
  has_many :subscription_histories
  has_many :clients, through: :subscription_histories
  has_many :subscription_items
  has_many :products, through: :subscription_items, source: :itemeable, source_type: 'Product'
  has_many :services, through: :subscription_items, source: :itemeable, source_type: 'Service'

  enum status: { active: true, inactive: false }

  validates_presence_of :name, :status, :price
end
