class Product < ApplicationRecord
  has_many :subscriptions
  has_many :subscription_items, as: :itemeable
  has_many :subscription_items, through: :subscriptions

  validates_presence_of :name
end
