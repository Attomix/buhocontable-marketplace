class Client < ApplicationRecord
  has_many :subscription_histories
  has_many :subscriptions, through: :subscription_histories
  has_many :payments

  validates_presence_of :name, :last_name
end
