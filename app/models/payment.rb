class Payment < ApplicationRecord
  belongs_to :client
  has_and_belongs_to_many :subscription_histories

  validates_presence_of :date_payment, :amount
end
