require 'rails_helper'
require 'creators/subscription_history_creator'
RSpec.describe SubscriptionHistoryCreator do
  let!(:subscriptions) { create_list(:subscription, 1) }
  let!(:clients) { create_list(:client, 1) }
  let(:period) { [:monthly, :annual] }

  context "Creator layer" do
    it "subscription" do
      expect {
        SubscriptionHistoryCreator.new({
                                           client_id: clients.first.id,
                                           subscription_id: subscriptions.first.id,
                                           period: :monthly,
                                           date_subscription: Date.today,
                                           status: true
                                       }).call
      }.to change { SubscriptionHistory.count }.by(1)
    end
  end

end