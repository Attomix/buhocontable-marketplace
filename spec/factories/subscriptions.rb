FactoryBot.define do
  factory :subscription do
    name { Faker::Commerce.product_name }
    status { :active }
    price { Faker::Number.decimal }
  end
end