FactoryBot.define do
  factory :subscription_item do
    association :itemeable, factory: [:service, :product]
    association :subscription, factory: :subscription
  end
end