FactoryBot.define do
  factory :payment do
    association :client, factory: :client
    date_payment { Date.today }
    amount { Faker::Number.decimal }
    association :subscription_histories, factory: :subscription_history
  end
end