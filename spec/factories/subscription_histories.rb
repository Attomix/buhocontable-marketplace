FactoryBot.define do
  factory :subscription_history do
    association :client, factory: :client
    association :subscription, factory: :subscription
    period { :monthly }
    date_subscription { Date.today }
    status { :active }
  end
end