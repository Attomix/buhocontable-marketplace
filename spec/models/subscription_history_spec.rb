require 'rails_helper'

RSpec.describe SubscriptionHistory, type: :model do
  client = Client.create!(name: Faker::Name.name, last_name:Faker::Name.last_name)

  subscription = Subscription.create!(
      name: Faker::Commerce.product_name,
      status: :active,
      price: Faker::Number.decimal)

  it "is valid with valid attributes" do
    subscription_history = described_class.new(
        client: client,
        subscription: subscription,
        period: :monthly,
        date_subscription: Date.today,
        status: :active)

    expect(subscription_history).to be_valid
  end



  it "is not valid without valid attributes" do
    subscription_history = described_class.new(
        client: nil,
        subscription: nil,
        period: nil,
        date_subscription: nil,
        status: nil)

    expect(subscription_history).to_not be_valid
  end
end
