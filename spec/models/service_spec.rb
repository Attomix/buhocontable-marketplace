require 'rails_helper'

RSpec.describe Service, type: :model do

  it "is valid with valid attributes" do
    product = described_class.new(name: Faker::Commerce.product_name)
    expect(product).to be_valid
  end

  it "is not valid without a name" do
    payment = described_class.new(name: nil)
    expect(payment).to_not be_valid
  end

end
