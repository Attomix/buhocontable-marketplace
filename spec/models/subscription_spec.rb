require 'rails_helper'

RSpec.describe Subscription, type: :model do

  it "is valid with valid attributes" do
    subscription = described_class.new(
        name: Faker::Commerce.product_name,
        status: :active,
        price: Faker::Number.decimal)

    expect(subscription).to be_valid
  end

  it "is not valid without a name" do
    subscription = described_class.new(
        name: nil,
        status: :active,
        price: Faker::Number.decimal)

    expect(subscription).to_not be_valid
  end

  it "is not valid without a status" do
    subscription = described_class.new(
        name: Faker::Commerce.product_name,
        status: nil,
        price: Faker::Number.decimal)

    expect(subscription).to_not be_valid
  end

  it "is not valid without a price" do
    subscription = described_class.new(
        name: Faker::Commerce.product_name,
        status: :active,
        price: nil)

    expect(subscription).to_not be_valid
  end

end
