require 'rails_helper'

RSpec.describe Payment, type: :model do

  client = Client.create!(name: Faker::Name.name, last_name: Faker::Name.last_name)

  it "is valid with valid attributes" do
    payment = described_class.new(
        client: client,
        date_payment: Date.today,
        amount: Faker::Number.decimal)

    expect(payment).to be_valid
  end

  it "is not valid without a client" do
    payment = described_class.new(
        client: nil,
        date_payment: Date.today,
        amount: Faker::Number.decimal)

    expect(payment).to_not be_valid
  end

  it "is not valid without a date_payment" do
    payment = described_class.new(
        client: client,
        date_payment: nil,
        amount: Faker::Number.decimal)

    expect(payment).to_not be_valid
  end

  it "is not valid without an amount" do
    payment = described_class.new(
        client: client,
        date_payment: Date.today,
        amount: nil)

    expect(payment).to_not be_valid
  end

end
