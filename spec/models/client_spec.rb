require 'rails_helper'

RSpec.describe Client, type: :model do

  it "is valid with valid attributes" do
    client = described_class.new(name: Faker::Name.name, last_name:Faker::Name.last_name)
    expect(client).to be_valid
  end

  it "is not valid without a name" do
    client = described_class.new(name: nil, last_name:Faker::Name.last_name)
    expect(client).to_not be_valid
  end

  it "is not valid without a last_name" do
    client = described_class.new(name: Faker::Name.name, last_name: nil)
    expect(client).to_not be_valid
  end

end
