require 'rails_helper'

RSpec.describe 'Clients API', type: :request do
  # initialize test data
  let!(:subscription) { create(:subscription) }
  let!(:client) { create(:client) }
  let!(:subscription_history) { create(:subscription_history, client: client, subscription: subscription) }
  let!(:payment) { create(:payment, client: client, subscription_histories: [subscription_history]) }

  # let(:subscription) { subscriptions.first.id }

  # Test suite for GET /api/v1/subscriptions
  describe 'GET /   api/v1/clients/payment_history' do

    before { get "/api/v1/clients/payment_history/#{client.id}" }

    it 'returns client payment history' do
      expect(json).not_to be_empty
      expect(response).to have_http_status(200)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

end