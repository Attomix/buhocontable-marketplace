require 'rails_helper'

RSpec.describe 'Subscription API', type: :request do
  # Test suite for POST /api/v1/subscriptions
  describe 'POST /api/v1/subscription_histories' do
    let!(:subscriptions) { create_list(:subscription, 5) }
    let!(:clients) { create_list(:client, 1) }
    let(:period) { [:monthly, :annual] }
    let(:valid_attributes) {{
        subscription_history: {
            client_id: clients.first.id,
            subscriptions: subscriptions.map{|sub| {id: sub.id, period: :monthly} },
        }
    }}

    context 'when request attributes are valid' do
      before { post "/api/v1/subscription_histories", params: valid_attributes }

      it 'returns status code 201' do
        puts valid_attributes.to_json
        expect(response).to have_http_status(201)
      end
    end

    # context 'when an invalid request' do
    #   before { post "/todos/#{todo_id}/items", params: {} }
    #
    #   it 'returns status code 422' do
    #     expect(response).to have_http_status(422)
    #   end
    #
    #   it 'returns a failure message' do
    #     expect(response.body).to match(/Validation failed: Name can't be blank/)
    #   end
    # end
  end
end