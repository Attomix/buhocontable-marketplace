require 'rails_helper'

RSpec.describe 'Subscription API', type: :request do
  # initialize test data
  let!(:product) { create(:product) }
  let!(:service) { create(:service) }
  let!(:subscriptions) { create_list(:subscription,2) }
  let!(:item_product){create(:subscription_item, itemeable: product, subscription_id: subscriptions.first.id)}
  let!(:item_service){create(:subscription_item, itemeable: service, subscription_id: subscriptions.first.id)}

  # let(:subscription) { subscriptions.first.id }

  # Test suite for GET /api/v1/subscriptions
  describe 'GET /api/v1/subscriptions' do

    before { get '/api/v1/subscriptions' }

    it 'returns subscription actives' do
      expect(json).not_to be_empty
      expect(json.size).to eq(2)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

end