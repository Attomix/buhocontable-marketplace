# README

Things you may want to cover:

* Ruby version 2.6.3

* System dependencies
    Ubuntu 18.04 +

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions
