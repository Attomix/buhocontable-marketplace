# frozen_string_literal: true

require File.expand_path(File.dirname(__FILE__) + "/environment")
env :PATH, ENV['PATH'] #Add this line
env :GEM_HOME, ENV["GEM_HOME"]
env :GEM_PATH, ENV['GEM_PATH']
set :output, {:standard => 'log/cron_success_log.log', :error => 'log/cron_error_log.log'}
set :environment, Rails.env.to_sym
set :bundle_command, ''

subscriptions = SubscriptionHistory.active
subscriptions.each do |item|
  date = item.date_subscription
  period = item.monthly? ? "0 0 #{date.day} * *" : "0 0 #{date.day} #{date.month} *"
  every period do
    rake "payments:make_payment[#{item.id}]"
  end
end