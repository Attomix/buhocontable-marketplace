Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  namespace :api do
    namespace :v1 do
      resources :subscriptions
      resources :subscription_histories
      resources :clients do
        collection do
          get 'payment_history/:id', to: 'clients#payment_history', as: 'payment_history'
        end
      end
    end
  end

end
