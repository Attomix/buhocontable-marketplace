class ResourceCreatorService
  attr_reader :errors

  def initialize(klass, data)
    @klass = klass
    @data = data
  end

  def call
    @klass.create!(@data)
  end
end
