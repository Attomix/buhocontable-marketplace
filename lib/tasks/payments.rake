namespace :payments do
  desc "automatic payments according to cut-off dates"
  task :make_payment ,[:id] =>[:environment] do |task , args|
    begin
    subscription_history_id = args[:id]
    subscription_history = SubscriptionHistory.find(subscription_history_id)
    payments = subscription_history.payments.where(date_payment: Date.today)
    return if (payments.count > 0) || (subscription_history.date_subscription == Date.today)

    price = subscription_history.subscription.price
    amount = subscription_history.monthly? ? price : price*12

    payment =  Payment.new(
        {
            client:subscription_history.client,
            date_payment: Date.today,
            amount: amount
        })
    subscription_history.payments.push(payment)
    subscription_history.save!

    rescue Exception => e
      puts e.message
    end
  end

end
