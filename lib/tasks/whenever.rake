namespace :whenever do
  desc "Update whenever"
  task crontab_update: :environment do
    WheneverRunner.update
  end

end
