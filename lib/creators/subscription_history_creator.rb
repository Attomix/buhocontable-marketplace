require "creators/creator"
require "services/resource_creator_service"

class SubscriptionHistoryCreator < Creator
  def create
    ResourceCreatorService.new(SubscriptionHistory, @data).call
  end
end
