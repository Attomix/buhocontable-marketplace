class Creator
  def initialize(data)
    @data = data
  end

  def create
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  def call
    record = create
    record
  end
end
