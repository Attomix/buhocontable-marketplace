# frozen_string_literal: true

module MarketPlace
  class ClientService

    def payment_histories(client)
      {
          client: serialize(client),
          payments_history: serialize(client.payments),
      }
    end

    private
    def serialize(data)
      ActiveModelSerializers::SerializableResource.new(data)
    end
  end
end
