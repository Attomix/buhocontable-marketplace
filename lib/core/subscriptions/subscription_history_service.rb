# frozen_string_literal: true

require 'creators/subscription_history_creator'

module MarketPlace
  class SubscriptionHistoryService

    def subscribe(data)
      record = SubscriptionHistoryCreator.new(parse_data(data)).call
      WheneverRunner.update
      record
    end

    private
    def parse_data(data)
      data[:subscriptions].each_with_object([]) do |subscription, form_data|
        form_data << {
            client_id: data[:client_id].to_i,
            subscription_id: subscription[:id].to_i,
            period: subscription[:period],
            date_subscription: Date.today,
            status: :active
        }
      end
    end
  end
end
