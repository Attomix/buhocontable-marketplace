class CreateSubscriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :subscriptions do |t|
      t.string :name
      t.boolean :status
      t.decimal :price, precision:10, scale: 2

      t.timestamps
    end
  end
end
