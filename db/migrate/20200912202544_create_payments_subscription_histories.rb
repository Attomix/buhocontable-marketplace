class CreatePaymentsSubscriptionHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :payments_subscription_histories, id: false do |t|
      t.belongs_to :payment
      t.belongs_to :subscription_history
    end
  end
end
