class CreateSubscriptionItems < ActiveRecord::Migration[6.0]
  def change
    create_table :subscription_items do |t|
      t.belongs_to :subscription
      t.references :itemeable, polymorphic: true

      t.timestamps
    end
  end
end
