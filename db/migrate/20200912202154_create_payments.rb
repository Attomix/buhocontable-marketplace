class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments do |t|
      t.belongs_to :client
      t.date :date_payment
      t.decimal :amount, precision:10, scale: 2

      t.timestamps
    end
  end
end
