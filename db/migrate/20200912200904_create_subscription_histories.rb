class CreateSubscriptionHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :subscription_histories do |t|
      t.belongs_to :client
      t.belongs_to :subscription
      t.integer :period
      t.date :date_subscription
      t.boolean :status

      t.timestamps
    end
  end
end
